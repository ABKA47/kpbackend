package com.klavyedeparmaklar.klavyedeparmaklar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KlavyedeparmaklarApplication {

	public static void main(String[] args) {
		SpringApplication.run(KlavyedeparmaklarApplication.class, args);
	}

}
