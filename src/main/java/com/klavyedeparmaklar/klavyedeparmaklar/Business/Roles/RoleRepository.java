package com.klavyedeparmaklar.klavyedeparmaklar.Business.Roles;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.klavyedeparmaklar.klavyedeparmaklar.Entities.Roles;


@Repository
public interface RoleRepository extends JpaRepository<Roles, Long> {
	Optional<Roles> findByName(String name);
}
