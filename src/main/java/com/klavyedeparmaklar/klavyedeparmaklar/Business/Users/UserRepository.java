package com.klavyedeparmaklar.klavyedeparmaklar.Business.Users;

import java.util.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.klavyedeparmaklar.klavyedeparmaklar.Entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	
	User findByUsername(String username);

	Boolean existsByUsername(String username);
	
	User findByUserId(Integer userId);
	
}	
