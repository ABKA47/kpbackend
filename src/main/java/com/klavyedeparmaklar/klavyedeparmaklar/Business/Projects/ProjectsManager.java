package com.klavyedeparmaklar.klavyedeparmaklar.Business.Projects;

import java.util.List;

import org.springframework.stereotype.Service;

import com.klavyedeparmaklar.klavyedeparmaklar.DataAccess.Projects.IProjectsDal;
import com.klavyedeparmaklar.klavyedeparmaklar.Entities.Projects;

@Service
public class ProjectsManager implements IProjectsService {
	
	private IProjectsDal projectDal;
	
	public ProjectsManager(IProjectsDal projectDal) {
		this.projectDal = projectDal;
	}

	@Override
	public void addProject(Projects project) {
		projectDal.addProject(project);		
	}

	@Override
	public List<Projects> getProject() {
		return projectDal.getProject();
	}

	@Override
	public void deleteProject(int projectId) {
		projectDal.deleteProject(projectId);
		
	}

	@Override
	public Projects findByProjectId(int projectId) {
		return projectDal.findByProjectId(projectId);
	}
	
	
}
