package com.klavyedeparmaklar.klavyedeparmaklar.Business.Projects;

import java.util.List;

import com.klavyedeparmaklar.klavyedeparmaklar.Entities.Projects;

public interface IProjectsService {
	void addProject(Projects project);
	public List<Projects> getProject();
	void deleteProject(int projectId);
	Projects findByProjectId(int projectId);
}
