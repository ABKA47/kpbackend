package com.klavyedeparmaklar.klavyedeparmaklar.Business.About;

import com.klavyedeparmaklar.klavyedeparmaklar.Entities.About;

import java.util.List;

public interface IAboutService  {
    void addAbout(About about);
    List<About> getAbouts();
    void deleteById(int aboutId);
    About findByAboutId(int aboutId);
}
