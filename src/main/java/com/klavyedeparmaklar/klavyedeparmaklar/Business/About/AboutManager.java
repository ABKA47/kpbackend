package com.klavyedeparmaklar.klavyedeparmaklar.Business.About;

import com.klavyedeparmaklar.klavyedeparmaklar.DataAccess.About.IAboutDal;
import com.klavyedeparmaklar.klavyedeparmaklar.Entities.About;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AboutManager implements IAboutService {
    private IAboutDal aboutDal;

    @Autowired
    public AboutManager(IAboutDal aboutDal) {
        this.aboutDal = aboutDal;
    }

    @Override
    public void addAbout(About about) {
        aboutDal.addAbout(about);
    }

    @Override
    public List<About> getAbouts() {
        return aboutDal.getAbouts();
    }

	@Override
	public void deleteById(int aboutId) {
		aboutDal.deleteById(aboutId);
		
	}

	@Override
	public About findByAboutId(int aboutId) {
		return aboutDal.findByAboutId(aboutId);
	}
}
