package com.klavyedeparmaklar.klavyedeparmaklar.Controller.ProjectsController;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.klavyedeparmaklar.klavyedeparmaklar.Business.Projects.IProjectsService;
import com.klavyedeparmaklar.klavyedeparmaklar.Entities.Projects;
import com.klavyedeparmaklar.klavyedeparmaklar.Payload.Request.ProjectsRequest;


@CrossOrigin("http://localhost:3000")
@RestController
@RequestMapping("/projects")
public class ProjectsController {
			
	private IProjectsService projectsService;
	String imageDirectory = new File("D:\\Projeler\\KlavyedeParmaklar.com\\FrontEnd\\klavyedeparmaklar\\public\\images\\Projects").toString();	
	
	@Autowired
	public ProjectsController(IProjectsService projectsService) {
		this.projectsService = projectsService;
	}
	
	@PostMapping("/addproject")
	public void addProjects(@Valid ProjectsRequest projectRequest) throws IOException{
			
		Projects project = new Projects();
		
		if(projectRequest.getProjectData() == null) {
			project.setProjectName(projectRequest.getProjectName());
			project.setProjectContent(projectRequest.getProjectContent());
			projectsService.addProject(project);
	
		}else {
			MultipartFile multipartFile = projectRequest.getProjectData();
			project.setProjectName(projectRequest.getProjectName());
			project.setProjectContent(projectRequest.getProjectContent());
			Path filePathName = Paths.get(imageDirectory,multipartFile.getOriginalFilename().concat(".").concat(FilenameUtils.getExtension(multipartFile.getOriginalFilename())));
	        Files.write(filePathName, multipartFile.getBytes());
	        project.setProjectFileName(multipartFile.getOriginalFilename());
	        project.setProjectFileType(FilenameUtils.getExtension(multipartFile.getOriginalFilename()));
			
	        project.setProjectData(multipartFile.getBytes());
			projectsService.addProject(project);
		}			
					
	}
	@GetMapping("/getproject")
	public List<Projects> getProject(){
		
		return projectsService.getProject();
		
	}
	@PostMapping("/editproject")
	public void editProject(@Valid ProjectsRequest projectRequest) throws IOException {
		
		Projects project = projectsService.findByProjectId(projectRequest.getProjectId());
		
		if(projectRequest.getProjectData() == null) {
			project.setProjectName(projectRequest.getProjectName());
			project.setProjectContent(projectRequest.getProjectContent());
			projectsService.addProject(project);
	
		}else {
			MultipartFile multipartFile = projectRequest.getProjectData();
			project.setProjectName(projectRequest.getProjectName());
			project.setProjectContent(projectRequest.getProjectContent());
			Path filePathName = Paths.get(imageDirectory,multipartFile.getOriginalFilename().concat(".").concat(FilenameUtils.getExtension(multipartFile.getOriginalFilename())));
	        Files.write(filePathName, multipartFile.getBytes());
	        project.setProjectFileName(multipartFile.getOriginalFilename());
	        project.setProjectFileType(multipartFile.getContentType());
			project.setProjectData(multipartFile.getBytes());
		
			projectsService.addProject(project);
		}			
					
	}
	@PostMapping("/deleteproject")
	public void deleteProject(@Valid @RequestBody Projects project) {
		projectsService.deleteProject(project.getProjectId());
	}
}
