package com.klavyedeparmaklar.klavyedeparmaklar.Controller.AuthController;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.klavyedeparmaklar.klavyedeparmaklar.Business.Roles.RoleRepository;
import com.klavyedeparmaklar.klavyedeparmaklar.Business.Users.UserRepository;
import com.klavyedeparmaklar.klavyedeparmaklar.Entities.Roles;
import com.klavyedeparmaklar.klavyedeparmaklar.Entities.User;
import com.klavyedeparmaklar.klavyedeparmaklar.Payload.Request.EditUserRequest;
import com.klavyedeparmaklar.klavyedeparmaklar.Payload.Request.LoginRequest;
import com.klavyedeparmaklar.klavyedeparmaklar.Payload.Request.RefreshTokenRequest;
import com.klavyedeparmaklar.klavyedeparmaklar.Payload.Request.SignUpRequest;
import com.klavyedeparmaklar.klavyedeparmaklar.Payload.Response.JwtResponse;
import com.klavyedeparmaklar.klavyedeparmaklar.Payload.Response.MessageResponse;
import com.klavyedeparmaklar.klavyedeparmaklar.Security.Jwt.JwtUtils;
import com.klavyedeparmaklar.klavyedeparmaklar.Security.Services.UserDetailsImpl;
import com.klavyedeparmaklar.klavyedeparmaklar.Security.Services.UserDetailsServiceImpl;

@RestController
@CrossOrigin(origins ="http://localhost:3000")
@RequestMapping("/auth")
public class AuthController {
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder encoder;

	@Autowired
	JwtUtils jwtUtils;
	
	@Autowired
	private UserDetailsServiceImpl userDetailsService;
	
	User user;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getUserName(), loginRequest.getUserPassword()));
		
		SecurityContextHolder.getContext().setAuthentication(authentication);
		String jwt = jwtUtils.generateJwtToken(authentication);
		int expires = jwtUtils.expiresIn();
		String refreshjwt = jwtUtils.doGenerateRefreshToken(authentication);

		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new JwtResponse(jwt, refreshjwt, expires, userDetails.getId(),
				userDetails.getUsername(), roles));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) {
		if (userRepository.existsByUsername(signUpRequest.getUserName())) {
			return ResponseEntity.badRequest().body(new MessageResponse("Error: Username is already taken!"));
		}
		
		// Create new user's account
		User user = new User(signUpRequest.getUserName(), encoder.encode(signUpRequest.getUserPassword()));
		
		String strRoles = signUpRequest.getRole();
		
		Set<Roles> roles = new HashSet<>();

		if (strRoles == null) {
			Roles userRole = roleRepository.findByName("ROLE_USER")
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			switch (strRoles) {
			case "ROLE_THEBOSS":
				Roles msoRole = roleRepository.findByName("ROLE_THEBOSS")
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(msoRole);

				break;
			case "ROLE_DEVELOPER":
				Roles itRole = roleRepository.findByName("ROLE_DEVELOPER")
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(itRole);

				break;
			case "ROLE_USER":
				Roles hrRole = roleRepository.findByName("ROLE_USER")
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(hrRole);

				break;
			default:
				Roles userRole = roleRepository.findByName("ROLE_USER")
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(userRole);
			}

		}
		
		user.setRoles(roles);

		userRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}
	

	@PostMapping("/refreshToken")
	public ResponseEntity<?> refreshtoken(@Valid @RequestBody RefreshTokenRequest refreshToken ) throws Exception {
		
		String username = jwtUtils.getUserNameFromJwtToken(refreshToken.getRefreshToken());
		
		UserDetails userDetail = userDetailsService.loadUserByUsername(username);
		
		UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
		userDetail, null, userDetail.getAuthorities());
		
		System.out.println("autheenticaaate: "+ authentication);
		
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		String jwt = jwtUtils.generateJwtToken(authentication);
		int expires= jwtUtils.expiresIn();
		String refreshjwt=jwtUtils.doGenerateRefreshToken(authentication);
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		List<String> roles = userDetails.getAuthorities().stream().map(item -> item.getAuthority())
				.collect(Collectors.toList());
		
		return ResponseEntity.ok(
				new JwtResponse(jwt,refreshjwt,expires, userDetails.getId(), userDetails.getUsername(), roles));
	}
	@GetMapping("/fetchusers")
	public List<User> fetchUsers(){
		List<User> userList = userRepository.findAll();
		return userList;
		
	}
	
	@PostMapping("/deleteuser")
	public ResponseEntity<?> deleteUser(@Valid @RequestBody User user){
		
		userRepository.deleteById(user.getUserId());
		
		return ResponseEntity.ok(new MessageResponse("User Deleted Successfully"));
	}
	@PostMapping("/edituser")
	public ResponseEntity<?> editUser(@Valid @RequestBody EditUserRequest editUser) {
		// Create new user's account
		User user = userRepository.findByUserId(editUser.getUserId());
		
		user.setUserName(editUser.getUserName());
		user.setUserPassword(encoder.encode(editUser.getUserPassword()));
		
		String strRoles = editUser.getRole();
		
		Set<Roles> roles = new HashSet<>();

		if (strRoles == null) {
			Roles userRole = roleRepository.findByName("ROLE_USER")
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			switch (strRoles) {
			case "ROLE_THEBOSS":
				Roles msoRole = roleRepository.findByName("ROLE_THEBOSS")
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(msoRole);

				break;
			case "ROLE_DEVELOPER":
				Roles itRole = roleRepository.findByName("ROLE_DEVELOPER")
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(itRole);

				break;
			case "ROLE_USER":
				Roles hrRole = roleRepository.findByName("ROLE_USER")
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(hrRole);

				break;
			default:
				Roles userRole = roleRepository.findByName("ROLE_USER")
						.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
				roles.add(userRole);
			}

		}
		
		user.setRoles(roles);

		userRepository.save(user);

		return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
	}
}
