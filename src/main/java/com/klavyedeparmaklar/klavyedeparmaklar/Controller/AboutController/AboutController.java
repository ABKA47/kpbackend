package com.klavyedeparmaklar.klavyedeparmaklar.Controller.AboutController;

import com.klavyedeparmaklar.klavyedeparmaklar.Business.About.IAboutService;
import com.klavyedeparmaklar.klavyedeparmaklar.Entities.About;
import com.klavyedeparmaklar.klavyedeparmaklar.Payload.Request.AboutRequest;
import com.klavyedeparmaklar.klavyedeparmaklar.Payload.Response.MessageResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@CrossOrigin("http://localhost:3000")
@RestController
@RequestMapping("/about")
public class AboutController {
    private IAboutService aboutService;
    String imageDirectory = new File("D:\\Projeler\\KlavyedeParmaklar.com\\FrontEnd\\klavyedeparmaklar\\public\\images\\Abouts").toString();
    //private static String imageDirectory = System.getProperty("C:\\Users\\Abdullah\\Desktop\\images");
    @Autowired
    public AboutController(IAboutService aboutService) {
        this.aboutService = aboutService;
    }

    @PostMapping("/addabout")
        public void  addAbout(@Valid AboutRequest aboutRequest) throws IOException {
        About about = new About();

        if(aboutRequest.getAboutData() == null){
            about.setAboutName(aboutRequest.getAboutName());
            about.setAboutTitle(aboutRequest.getAboutTitle());
            about.setAboutContent(aboutRequest.getAboutContent());

            aboutService.addAbout(about);
        }

        else{
            about.setAboutName(aboutRequest.getAboutName());
            about.setAboutTitle(aboutRequest.getAboutTitle());
            about.setAboutContent(aboutRequest.getAboutContent());
            MultipartFile multipartFile = aboutRequest.getAboutData();
           
            Path filePathName = Paths.get(imageDirectory,multipartFile.getOriginalFilename().concat(".").concat(FilenameUtils.getExtension(multipartFile.getOriginalFilename())));
            Files.write(filePathName, multipartFile.getBytes());
            about.setAboutFileName(multipartFile.getOriginalFilename());
            about.setAboutFileType(FilenameUtils.getExtension(multipartFile.getOriginalFilename()));
            
            about.setAboutData(multipartFile.getBytes());
            aboutService.addAbout(about);
        }
    }

    @GetMapping("/getabouts")
    public List<About> getAbouts(){
        return aboutService.getAbouts();
    }
    @PostMapping("/deleteabout")
    public ResponseEntity<?> deleteAbout(@Valid @RequestBody About about){
		aboutService.deleteById(about.getAboutId());
		
		return ResponseEntity.ok(new MessageResponse("User Deleted Successfully"));
	}
    @PostMapping("/editabout")
    public void editAbout(@Valid AboutRequest about) throws IOException {
    	  About editabout = aboutService.findByAboutId(about.getAboutId());

          if(about.getAboutData() == null){
              editabout.setAboutName(about.getAboutName());
              editabout.setAboutTitle(about.getAboutTitle());
              editabout.setAboutContent(about.getAboutContent());

              aboutService.addAbout(editabout);
          }

          else{
              editabout.setAboutName(about.getAboutName());
              editabout.setAboutTitle(about.getAboutTitle());
              editabout.setAboutContent(about.getAboutContent());
              MultipartFile multipartFile = about.getAboutData();
             
              Path filePathName = Paths.get(imageDirectory,multipartFile.getOriginalFilename().concat(".").concat(FilenameUtils.getExtension(multipartFile.getOriginalFilename())));
              Files.write(filePathName, multipartFile.getBytes());
              editabout.setAboutFileName(multipartFile.getOriginalFilename());
              editabout.setAboutFileType(FilenameUtils.getExtension(multipartFile.getOriginalFilename()));
              
              editabout.setAboutData(multipartFile.getBytes());
              aboutService.addAbout(editabout);
          }
    }
}
