package com.klavyedeparmaklar.klavyedeparmaklar.DataAccess.About;

import com.klavyedeparmaklar.klavyedeparmaklar.Entities.About;

import java.util.List;

public interface IAboutDal {
    void addAbout(About about);
    List<About> getAbouts();
    void deleteById(int aboutId);
    About findByAboutId(int aboutId);
}
