package com.klavyedeparmaklar.klavyedeparmaklar.DataAccess.About;

import com.klavyedeparmaklar.klavyedeparmaklar.Entities.About;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class AboutDal implements IAboutDal {
    private EntityManager entityManager;
    
    @Autowired
    public AboutDal(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    @Transactional
    public void addAbout(About about) {
        Session session = entityManager.unwrap(Session.class);
        session.saveOrUpdate(about);
    }

    @Override
    @Transactional
    public List<About> getAbouts() {
        Session session = entityManager.unwrap(Session.class);
        List<About> aboutList = session.createQuery("from About", About.class).getResultList();
        return aboutList;
    }

	@Override
	@Transactional
	public void deleteById(int aboutId) {
		 Session session = entityManager.unwrap(Session.class);
		 About about = (About) session.get(About.class, aboutId);
		 session.delete(about);
						
	}

	@Override
	@Transactional
	public About findByAboutId(int aboutId) {
		Session session = entityManager.unwrap(Session.class);
		About about = (About) session.get(About.class, aboutId);
		return about;
	}
}
