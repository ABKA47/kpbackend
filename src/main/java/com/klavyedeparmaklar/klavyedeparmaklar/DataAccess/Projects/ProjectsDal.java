package com.klavyedeparmaklar.klavyedeparmaklar.DataAccess.Projects;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.klavyedeparmaklar.klavyedeparmaklar.Entities.Projects;

@Repository
public class ProjectsDal implements IProjectsDal{
	
	private EntityManager entityManager;
	
	public ProjectsDal(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

		
	@Override
	@Transactional
	public void addProject(Projects project) {
		Session session = entityManager.unwrap(Session.class);
		session.saveOrUpdate(project);
	}

	@Override
	@Transactional
	public List<Projects> getProject() {
		Session session = entityManager.unwrap(Session.class);
		List<Projects> projects = session.createQuery(" from Projects",Projects.class).getResultList();
		return projects;
	}


	@Override
	@Transactional
	public void deleteProject(int projectId) {
		Session session = entityManager.unwrap(Session.class);
		Projects project = session.get(Projects.class, projectId);
		session.delete(project);
		
	}


	@Override
	@Transactional
	public Projects findByProjectId(int projectId) {
		Session session = entityManager.unwrap(Session.class);
		Projects project = session.get(Projects.class, projectId);
		return project;
	}

}
