package com.klavyedeparmaklar.klavyedeparmaklar.DataAccess.Projects;

import java.util.List;

import com.klavyedeparmaklar.klavyedeparmaklar.Entities.Projects;

public interface IProjectsDal {
	void addProject(Projects project);
	public List<Projects> getProject();
	void deleteProject(int projectId);
	Projects findByProjectId(int projectId);
}
