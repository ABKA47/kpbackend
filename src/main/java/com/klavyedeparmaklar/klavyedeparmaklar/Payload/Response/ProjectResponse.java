package com.klavyedeparmaklar.klavyedeparmaklar.Payload.Response;

import org.springframework.web.multipart.MultipartFile;

public class ProjectResponse {
	
	private int project_id;
	private String project_name;
	private String project_content;
	private MultipartFile project_data;
	
	public ProjectResponse(int project_id, String project_name, String project_content, MultipartFile project_data) {
		this.project_id = project_id;
		this.project_name = project_name;
		this.project_content = project_content;
		this.project_data = project_data;
	}

	public int getProject_id() {
		return project_id;
	}

	public void setProject_id(int project_id) {
		this.project_id = project_id;
	}

	public String getProject_name() {
		return project_name;
	}

	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}

	public String getProject_content() {
		return project_content;
	}

	public void setProject_content(String project_content) {
		this.project_content = project_content;
	}

	public MultipartFile getProject_data() {
		return project_data;
	}

	public void setProject_data(MultipartFile project_data) {
		this.project_data = project_data;
	}
}
