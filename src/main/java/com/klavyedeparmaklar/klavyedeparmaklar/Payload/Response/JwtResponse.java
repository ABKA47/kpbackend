package com.klavyedeparmaklar.klavyedeparmaklar.Payload.Response;

import java.util.List;

public class JwtResponse {
	
	private String token;
	private String refreshToken;
	private String type = "Bearer";
	private int id;
	private String userName;
	private List<String> roles;
	private int expiresIn;
	
	public JwtResponse(String accessToken,String refreshToken,int expires,int id, String userName,  List<String> roles) {
		this.token = accessToken;
		this.id = id;
		this.userName = userName;
		this.roles = roles;
		this.expiresIn=expires;
		this.refreshToken=refreshToken;
	}

	public String getAccessToken() {
		return token;
	}

	public void setAccessToken(String accessToken) {
		this.token = accessToken;
	}

	public String getTokenType() {
		return type;
	}

	public void setTokenType(String tokenType) {
		this.type = tokenType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return userName;
	}

	public void setUsername(String userName) {
		this.userName = userName;
	}

	public List<String> getRoles() {
		return roles;
	}

	public int getExpiresIn() {
		return expiresIn;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	
	

}
