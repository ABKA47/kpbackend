package com.klavyedeparmaklar.klavyedeparmaklar.Payload.Request;

import javax.validation.constraints.NotBlank;

public class SignUpRequest {
	
	@NotBlank
	private String userName;
	
	@NotBlank
	private String userPassword;
	
	@NotBlank
	private String role;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getUserPassword() {
		return userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
	
	

}
