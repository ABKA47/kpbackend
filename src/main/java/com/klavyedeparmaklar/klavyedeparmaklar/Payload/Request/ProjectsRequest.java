package com.klavyedeparmaklar.klavyedeparmaklar.Payload.Request;

import org.springframework.web.multipart.MultipartFile;

public class ProjectsRequest {	
	int projectId;
	String projectName;
	String projectContent;
	MultipartFile projectData;
	
    public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProjectContent() {
		return projectContent;
	}
	public void setProjectContent(String projectContent) {
		this.projectContent = projectContent;
	}
	public MultipartFile getProjectData() {
		return projectData;
	}
	public void setProjectData(MultipartFile projectData) {
		this.projectData = projectData;
	}
	
	
	
	
}
