package com.klavyedeparmaklar.klavyedeparmaklar.Payload.Request;

import org.springframework.web.multipart.MultipartFile;

public class AboutRequest {
	int aboutId;
    String aboutName;
    String aboutTitle;
    String aboutContent;
    MultipartFile aboutData;

    public int getAboutId() {
		return aboutId;
	}

	public void setAboutId(int aboutId) {
		this.aboutId = aboutId;
	}

	public String getAboutName() {
        return aboutName;
    }

    public void setAboutName(String aboutName) {
        this.aboutName = aboutName;
    }

    public String getAboutTitle() {
        return aboutTitle;
    }

    public void setAboutTitle(String aboutTitle) {
        this.aboutTitle = aboutTitle;
    }

    public String getAboutContent() {
        return aboutContent;
    }

    public void setAboutContent(String aboutContent) {
        this.aboutContent = aboutContent;
    }

    public MultipartFile getAboutData() {
        return aboutData;
    }

    public void setAboutData(MultipartFile aboutData) {
        this.aboutData = aboutData;
    }
}
