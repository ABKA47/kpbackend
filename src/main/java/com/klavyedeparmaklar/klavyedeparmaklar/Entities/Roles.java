package com.klavyedeparmaklar.klavyedeparmaklar.Entities;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

	@Entity
	@Table(name = "roles")
	public class Roles {
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		@Column(name = "roleid")
		private int id;

		
		@Column(name = "rolename")
		private String name;

		public Roles() {

		}

		public Roles(String name) {
			this.name = name;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}
	}


