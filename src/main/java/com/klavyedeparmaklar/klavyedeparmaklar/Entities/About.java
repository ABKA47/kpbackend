package com.klavyedeparmaklar.klavyedeparmaklar.Entities;

import javax.persistence.*;

@Table(name="about")
@Entity
public class About {
	
	public About() {}		
	public About(int aboutId, String aboutName, String aboutTitle, String aboutContent, String aboutFileName,
			String aboutFileType, byte[] aboutData) {
	
		this.aboutId = aboutId;
		this.aboutName = aboutName;
		this.aboutTitle = aboutTitle;
		this.aboutContent = aboutContent;
		this.aboutFileName = aboutFileName;
		this.aboutFileType = aboutFileType;
		this.aboutData = aboutData;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "aboutid")
	private int aboutId;
	
	@Column(name="aboutname")
	private String aboutName;
	
	@Column(name="abouttitle")
	private String aboutTitle;
	
	@Column(name="aboutcontent")
	private String aboutContent;
	
	@Column(name ="aboutfilename")
	private String aboutFileName;
	
	@Column(name ="aboutfiletype")
	private String aboutFileType;

	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name="aboutdata")
	private byte[] aboutData;

	public String getAboutFileType() {
		return aboutFileType;
	}

	public void setAboutFileType(String aboutFileType) {
		this.aboutFileType = aboutFileType;
	}

	public String getAboutFileName() {
		return aboutFileName;
	}

	public void setAboutFileName(String aboutFileName) {
		this.aboutFileName = aboutFileName;
	}

	public byte[] getAboutData() {
		return aboutData;
	}

	public void setAboutData(byte[] aboutData) {
		this.aboutData = aboutData;
	}

	public int getAboutId() {
		return aboutId;
	}

	public void setAboutId(int aboutId) {
		this.aboutId = aboutId;
	}

	public String getAboutName() {
		return aboutName;
	}

	public void setAboutName(String aboutName) {
		this.aboutName = aboutName;
	}

	public String getAboutTitle() {
		return aboutTitle;
	}

	public void setAboutTitle(String aboutTitle) {
		this.aboutTitle = aboutTitle;
	}

	public String getAboutContent() {
		return aboutContent;
	}

	public void setAboutContent(String aboutContent) {
		this.aboutContent = aboutContent;
	}
}
