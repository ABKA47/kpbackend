package com.klavyedeparmaklar.klavyedeparmaklar.Entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name="projects")
public class Projects {
	
	public Projects() {}	
	
	public Projects(int projectId, String projectName, String projectContent, String projectFileName,
			String projectFileType, byte[] projectData) {
		
		this.projectId = projectId;
		this.projectName = projectName;
		this.projectContent = projectContent;
		this.projectFileName = projectFileName;
		this.projectFileType = projectFileType;
		this.projectData = projectData;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "projectid")
	private int projectId;
	
	@Column(name="projectname")
	private String projectName;
	
	@Column(name="projectcontent")
	private String projectContent;
	@Column(name="projectfilename")
	private String projectFileName;
	@Column(name="projectfiletype")
	private String projectFileType;
	@Lob
	@Basic(fetch = FetchType.LAZY)
	@Column(name="projectdata")
	private byte[] projectData;

	
	public String getProjectFileName() {
		return projectFileName;
	}

	public void setProjectFileName(String projectFileName) {
		this.projectFileName = projectFileName;
	}

	public String getProjectFileType() {
		return projectFileType;
	}

	public void setProjectFileType(String projectFileType) {
		this.projectFileType = projectFileType;
	}

	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectContent() {
		return projectContent;
	}

	public void setProjectContent(String projectContent) {
		this.projectContent = projectContent;
	}

	public byte[] getProjectData() {
		return projectData;
	}

	public void setProjectData(byte[] projectData) {
		this.projectData = projectData;
	}

	
}
